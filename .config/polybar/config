;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
black = #1E222A
black-bright = #333945
red = #E06C75 
red-bright = #FF7A85
green = #98C379
green-bright = #B5E890
yellow = #E5C07B
yellow-bright = #FFD68A
blue = #61AFEF
blue-bright = #69BBFF
magenta = #C678DD
magenta-bright = #E48AFF
cyan = #56B6C2
cyan-bright =  #66D9E8
white = #ABB2BF
white-bright = #CFD7E6

[bar/top]
enable-ipc = true
;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 22
;offset-x = 1%
;offset-y = 1%
;radius = 6.0
fixed-center = true

;Bar background color
background = ${colors.black}
; Text color
foreground = ${colors.white}

line-size = 2

; Controll the border
;border-size = 0
border-top-size = 5
border-bottom-size = 5
border-top-color = ${colors.black}
border-bottom-color = ${colors.black}

padding-left = 2
padding-right = 2

font-0 = Hack Nerd Font:style=Bold:pixelsize=10;3
font-1 = Hack Nerd Font:size=15;4
font-2 = Material Design Icons:style=Bold:size=12;3
font-3 = unifont:fontformat=truetype:size=13:antialias=true;

modules-left = round-left bspwm round-right
modules-center =
modules-right =  temperature alsa xkeyboard space round-left cpu round-right updates round-left time round-right powermenu

;tray-position = right
;tray-padding = 2
;tray-background = #0063ff

wm-restack = bspwm

override-redirect = false

tray-detached = false

cursor-click = pointer
cursor-scroll = ns-resize


[module/round-left]
type = custom/text
content = %{T2}%{T-}
content-foreground = ${colors.black-bright}

[module/round-right]
type = custom/text
content = %{T2}%{T-}
content-foreground = ${colors.black-bright}


[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = "󰥻 "
format-prefix-foreground = ${colors.green-bright}

label-layout = %layout%

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}


[module/bspwm]
type = internal/bspwm

pin-workspaces = true
inline-mode = true
enable-click = true


format = <label-state>

ws-icon-0 = 1;%{F#FFD68A}󰆍
ws-icon-1 = 2;%{F#FF7A85}󰖟
ws-icon-2 = 3;%{F#66D9E8}󰗚
ws-icon-3 = 4;%{F#61AFEF}󰙯
ws-icon-4 = 5;%{F#B5E890}󰓇

label-separator = ""
label-separator-background = ${colors.black-bright}

label-focused = %icon%
label-focused-foreground = ${colors.white-bright}
label-focused-background = ${colors.black-bright}
label-focused-underline= ${colors.white}
label-focused-padding = 1

label-occupied = %icon%
label-occupied-padding = 1
label-occupied-background = ${colors.black-bright}
label-occupied-foreground = ${colors.white-bright}

label-urgent = %icon%!
label-urgent-background = ${colors.black-bright}
label-urgent-foreground = ${colors.white-bright}
label-urgent-padding = 1

label-empty = %icon%
label-empty-background = ${colors.black-bright}
label-empty-foreground = ${colors.white-bright}
label-empty-padding = 1

[module/space]
type = custom/text
content = " "

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = "  "
format = <label>
label = CPU %percentage%%
format-background = ${colors.black-bright}
format-foreground = #989cff


[module/time]
type = internal/date
interval = 60
format = <label>
format-background = ${colors.black-bright}
format-prefix = "󰃰"
format-prefix-foreground = ${colors.red-bright}
time = %{F${colors.white-bright} Week %U | %a %Y-%m-%d | %H:%M%{F-}

label = %time%

[module/updates]
type = custom/script
exec = checkupdates | wc -l
format = <label>
interval = 4600
label = %output% Updates
label-padding = 2
label-foreground = ${colors.yellow-bright}

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = %{T3}󰕾%{T-}
label-volume-foreground = ${colors.white-bright}

format-muted-foreground = ${colors.white}
label-muted = 󰖁 Muted

bar-volume-width = 10
bar-volume-foreground-0 = ${colors.blue-bright}
bar-volume-foreground-1 = ${colors.blue-bright}
bar-volume-foreground-2 = ${colors.blue-bright}
bar-volume-foreground-3 = ${colors.blue-bright}
bar-volume-foreground-4 = ${colors.blue-bright}
bar-volume-foreground-5 = ${colors.blue-bright}
bar-volume-foreground-6 = ${colors.blue-bright}
bar-volume-gradient = false
bar-volume-indicator =
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 1
bar-volume-empty = ─
bar-volume-empty-font = 1
bar-volume-empty-foreground = ${colors.white}
format-volume-padding = 2
format-muted-padding = 2

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
