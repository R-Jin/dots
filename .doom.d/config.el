(setq user-full-name "Ryan Jin"
      user-mail-address "ryanjin007@gmail.com")

(setq doom-font (font-spec :family "Hack Nerd Font" :size 14 :weight 'semi-light)
    doom-variable-pitch-font (font-spec :family "Hack Nerd Font") ; inherits `doom-font''s :size
    doom-unicode-font (font-spec :family "Hack Nerd Font" :size 14)
    doom-big-font (font-spec :family "Hack Nerd Font" :size 19))

(setq doom-theme 'doom-one)

(setq all-the-icons-scale-factor 0.8)

(setq org-directory "~/org/")

(use-package! org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(setq display-line-numbers-type 'relative)

(use-package! highlight-indent-guides
  :commands highlight-indent-guides-mode
  :hook (prog-mode . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-method 'character
        highlight-indent-guides-character ?\|
        highlight-indent-guides-delay 0.01
        highlight-indent-guides-responsive 'top
        highlight-indent-guides-auto-enabled t))

(setq ccls-sem-highlight-method 'font-lock)

(cmake-ide-setup)

(setq haskell-stylish-on-save t)

(add-to-list 'company-backends 'company-ghc)
